{-# LANGUAGE OverloadedStrings  #-}

--
module BakingSoda.Gen where
--
import BakingSoda.Model
--
import Text.Read (readEither)
import qualified Text.Layout.Table as TL
--
import qualified Data.Bool as BL
import qualified Data.Bifunctor as B
import qualified Data.List as L
import qualified Data.HashMap.Strict as H
import qualified Data.Either as E
import qualified Data.String as S
import qualified Data.Text as T
import qualified Data.Maybe as M
import qualified Data.Set as S
--
import System.IO
--

data Table
    = Table
    { tableName      :: T.Text
    , tablefield     :: H.HashMap Group [ TableField ]
    , tableUniqueKey :: T.Text
    } deriving (Show)
--
data TableField
    = TableField
    { fieldName    :: T.Text
    , fieldType    :: T.Text
    , fieldIsIndex :: Bool
    , fieldModifyRule :: ModifyRule
    , fieldRule       :: [ Rule VariableType ]
    } deriving (Show, Ord, Eq)
--

verifyConfig
  :: String
  -> IO ()
verifyConfig p = do
  eCs <- configIO p
  let cs :: Either ErrorInfo [Config]
      -- do not modify for now
      -- cs = mapM (>>= modifyRule) eCs
      cs = sequence eCs

  case cs of
    Left e -> hPutStrLn stderr e
    Right rs -> do
      handler <- openFile p WriteMode
      hPutStrLn handler $ modelRuleReOutput rs
      hClose handler

modelRuleReOutput
  :: [ Config ]
  -> String
modelRuleReOutput ls = TL.gridString (columnSetting lenHeader) $ appendHeader $ map content ls
  where content c = [ show $ cId c
                    , show $ cIsIndex c
                    , show $ cGroups c
                    , toShowable $ show $ cParent c
                    , show $ cModifyRule c
                    , show $ cType c
                    , T.unpack $ database $ cStorage c
                    , T.unpack $ table $ cStorage c
                    , T.unpack $ cColumn c
                    , T.unpack $ showRulePath $ cRule c
                    , T.unpack $ cUniqueKey c
                    , M.maybe "-" T.unpack $ cReferenceDoc c
                    , toBase64 $ cRule c
                    ]
        lenHeader = length header

modifyRule :: Config -> Either ErrorInfo Config
modifyRule c = update <$> modifyRule' m t l
    where m = cModifyRule c
          t = cType c
          l = cRule c
          update :: Rule VariableType -> Config
          update r = c { cRule = r }

modifyRule'
  :: ModifyRule
  -> VDataType
  -> Rule VariableType
  -> Either ErrorInfo (Rule VariableType)
-- TODO: default one, need more description
modifyRule' Expand t r = Right r
modifyRule' List t r = Right r
-- TODO: no support now
modifyRule' (Index _) t r = Left "no support"
modifyRule' (Stop s) t r = cutRuleBy s t r

cutRuleBy
  :: T.Text
  -> VDataType
  -> Rule VariableType
  -> Either ErrorInfo (Rule VariableType)
cutRuleBy s t (Target r) = Left "target no found"
cutRuleBy s t (Cat h p r) = Cat h p <$> cutRuleBy s t r
cutRuleBy s t (Rule h p r)
   | s == p = Right $ Rule h p $ Target $ fromVDataType t
   | otherwise = Rule h p <$> cutRuleBy s t r

genHaskellTable
  :: String -- ^ the path of model
  -> String -- ^ the output file
  ->  IO ()
genHaskellTable p o = do
  eCs <- configIO p
  let cs = sequence eCs
      res :: Either ErrorInfo [ T.Text ]
      res = fmap (map genHaskellDataRaw . configToTable toHaskellDataType) cs
  case res of
    Left e -> putStrLn e
    Right xs -> mapM_ (putStrLn . T.unpack) xs

genHaskellDataField :: Int -> TableField -> T.Text
genHaskellDataField sp (TableField fname ftype _ _ _) =
   T.replace "/" "#" fname
   <> T.replicate (sp - T.length fname) " "
   <> " :: "
   <> ftype

snakeToCamelCaseDT :: T.Text -> T.Text
snakeToCamelCaseDT t = fstLower $ snakeToCamelCaseTP t

snakeToCamelCaseTP :: T.Text -> T.Text
snakeToCamelCaseTP t = T.intercalate "" $ map fstUpper $ T.splitOn "_" t

fstLower :: T.Text -> T.Text
fstLower "" = ""
fstLower x = T.toLower (T.take 1 x) <> T.tail x

fstUpper :: T.Text -> T.Text
fstUpper "" = ""
fstUpper x = T.toUpper (T.take 1 x) <> T.tail x

genHaskellDataRaw :: Table -> T.Text
genHaskellDataRaw (Table tname tfds tk) =
   let sp = (2 + fieldSpacing allFields)
       timestampField = TableField
           { fieldName    = "InsertedTimestamp"
           , fieldType    = "T.Text"
           , fieldIsIndex = True
           , fieldModifyRule = Expand
           , fieldRule       = []
           }
       allFields = L.sortOn fieldName
                 $ map dataname
                 $ (:) timestampField
                 $ foldr dedup []
                 $ concat
                 $ H.elems tfds
       typename = snakeToCamelCaseTP tname
       dataname f = f { fieldName
                          = snakeToCamelCaseDT
                             (fieldName f)}
       in
      T.concat
      [ "data " <> typename  <> " = " <> typename
      , fieldConcat Front $ map (genHaskellDataField sp) allFields
      , "\n    }"
      , "\n"
      ]

genCreateTableSql
  :: String -- ^ the path of model
  -> String -- ^ the output file
  ->  IO ()
genCreateTableSql p o = do
  eCs <- configIO p
  let cs = sequence eCs
      res :: Either ErrorInfo [ T.Text ]
      res = fmap (map genTable . configToTable toSqlDataType) cs

  putStrLn "output sql.."
  handle <- openFile o WriteMode
  case res of
    Left e -> hPutStrLn stderr e
    Right xs -> mapM_ (hPutStrLn handle . T.unpack) xs

  hClose handle

genTable :: Table -> T.Text
genTable tb =
   let tableString = genTableRaw tb
       indexString = genTableIdx tb
   in tableString <> "\n" <> indexString

numTableField :: TableField -> [ TableField ] -> Int
numTableField t fs
  = length
  $ filter (== fieldName t)
  $ map fieldName fs

dedup :: TableField -> [ TableField ] -> [ TableField ]
dedup a b = BL.bool b (a:b) (numTableField a b < 1)

--
genTableRaw :: Table -> T.Text
genTableRaw (Table tname tfds tk) =
   let sp = (2 + fieldSpacing allFields)
       timestampField = TableField
           { fieldName    = "_inserted_timestamp"
           , fieldType    = "timestamp without time zone NOT NULL\n"
                          <> "        DEFAULT (current_timestamp AT TIME ZONE 'UTC')"
           , fieldIsIndex = True
           , fieldModifyRule = Expand
           , fieldRule       = []
           }
       allFields = L.sortOn fieldName
                 $ (:) timestampField
                 $ foldr dedup []
                 $ concat
                 $ H.elems tfds
       in
      T.concat
      [ "CREATE TABLE " <> tname <> " ("
      , fieldConcat Back $ map (genTableField sp tk) allFields
      , ");"
      ]

data CommonPos = Front | Back
               deriving Show
--
fieldConcat :: CommonPos -> [T.Text] -> T.Text
fieldConcat _ [] = ""
fieldConcat Back [x] = "\n    " <> x
fieldConcat Back (x:xs) =
   "\n    " <> x <> "," <> fieldConcat Back xs
fieldConcat Front (x:xs) = "\n    { " <> x <> fieldConcat' Front xs
    where fieldConcat' p [] = ""
          fieldConcat' p [x] = "\n    , " <> x
          fieldConcat' p (x':xs')
            = "\n    , " <> x' <> fieldConcat' p xs'
--
genTableField :: Int -> T.Text -> TableField -> T.Text
genTableField sp tk (TableField fname ftype isIdx _ _) =
   "\"" <> T.replace "/" "#" fname <> "\""
   <> T.replicate (sp - T.length fname) " "
   <> ftype
   <> hasUniqueKey
   where hasUniqueKey :: T.Text
         hasUniqueKey
           | tk == fname = " unique"
           | otherwise = T.empty
--
fieldSpacing :: [TableField] -> Int
fieldSpacing = foldr (max . T.length . fieldName) 0
--
genTableIdx :: Table -> T.Text
genTableIdx (Table tname tfds tk)
   = T.unlines
   $ map (genTableFieldIdx tname)
   $ filter fieldIsIndex allFields
   where allFields = L.sortOn fieldName
                   $ foldr dedup []
                   $ concat
                   $ H.elems tfds

genTableFieldIdx :: T.Text -> TableField -> T.Text
genTableFieldIdx tname (TableField tfn _ True _ _) = T.concat
   [ "CREATE INDEX "
   , quote (tname <> "_" <> tfn <> "_idx")
   , " ON ", tname
   , " (", quote tfn , ");"
   ]
   where quote x = "\"" <> x <> "\""
genTableFieldIdx _ _  = ""
--
--
data Config = Config
   { cId         :: Int
   , cIsIndex    :: Bool
   , cGroups     :: [ Group ]
   , cParent     :: Maybe Int
   , cModifyRule :: ModifyRule
   , cType       :: VDataType
   , cStorage    :: Storage
   , cColumn     :: T.Text
   , cPath       :: Maybe T.Text
   , cUniqueKey  :: T.Text
   , cReferenceDoc :: Maybe T.Text
   , cRule         :: Rule VariableType
   } deriving (Show)
--
type ErrorInfo = String

toSqlDataType :: VDataType -> String
toSqlDataType (VDArray v) = toSqlDataType v <> "[]"
toSqlDataType VDText   = "character varying"
toSqlDataType VDNumber = "float8"
toSqlDataType VDBool   = "bool"
toSqlDataType _ = undefined

toHaskellDataType :: VDataType -> String
toHaskellDataType (VDArray v) = "[ " <> toHaskellDataType v <> " ]"
toHaskellDataType VDText   = "T.Text"
toHaskellDataType VDNumber = "Double"
toHaskellDataType VDBool   = "Bool"
toHaskellDataType _ = undefined

configToTable :: (VDataType -> String) -> [ Config ] -> [ Table ]
configToTable tp cs = H.elems $ configToTable' H.empty cs
    where configToTable'
            :: H.HashMap T.Text Table
            -> [Config]
            -> H.HashMap T.Text Table
          configToTable' = foldl parsingConfig
          parsingConfig
            :: H.HashMap T.Text Table
            -> Config
            -> H.HashMap T.Text Table
          parsingConfig h x =
            case H.lookup (table $ cStorage x) h of
              Just v ->  H.insert (table $ cStorage x) (insertGroupTable tp x v) h
              Nothing -> H.insert (table $ cStorage x) (newTable tp x) h

newField :: (VDataType -> String) -> Config -> TableField
newField t c = TableField
           { fieldName = cColumn c
           , fieldType = T.pack $ t $ cType c
           , fieldIsIndex = cIsIndex c
           , fieldModifyRule = cModifyRule c
           , fieldRule = [ cRule c ]
           }

newTable :: (VDataType -> String) -> Config -> Table
newTable t c = Table
           { tableName = table $ cStorage c
           , tablefield = H.fromList
                        $ zip (cGroups c)
                        $ repeat [ newField t c ]
           , tableUniqueKey = cUniqueKey c
           }

insertGroupTable :: (VDataType -> String) -> Config -> Table -> Table
insertGroupTable tp c t = t { tablefield = insertGroups' }
    where existedGroup g = H.lookup g tf
          tf = tablefield t
          insertGroups' = foldr insertGroup' tf $ cGroups c
          insertGroup' :: Group
                       -> H.HashMap Group [ TableField ]
                       -> H.HashMap Group [ TableField ]
          insertGroup' g  h =
            case existedGroup g of
              Just v -> H.insert g (addFieldToTable tp c v) h
              Nothing -> H.insert g [ newField tp c ] h

addFieldToTable :: (VDataType -> String) -> Config -> [ TableField ] -> [ TableField ]
addFieldToTable tp c t =
    if existedTfn then
      addRuleToField (cRule c) tfn t
    else
      newTfs
    where tfn = cColumn c
          newTfs = newField tp c : t
          existedTfn = 0 /= length (filter (\x -> fieldName x == tfn) t)

addRuleToField :: Rule VariableType -> T.Text -> [ TableField ] -> [ TableField ]
addRuleToField r c [] = []
addRuleToField r c (t:ts)
  | fieldName t == c = t {fieldRule = r: fieldRule t} : ts
  | otherwise = t : addRuleToField r c ts

--
configIO :: String -> IO [Either ErrorInfo Config]
configIO path = do
   ctx <- readFile path
   let res = config ctx
   return res

config :: String -> [Either ErrorInfo Config]
config content
   = map (step . S.words)
   $ tail
   $ S.lines content
--
step :: [String] -> Either ErrorInfo Config
step x@[n,i,g,h,m,t,d,b,c,p,u,f,r] =
   toConfig =<< fromBase64 r
   where toConfig :: String -> Either ErrorInfo Config
         toConfig rule =
           Config
           <$> (eMsgEither n l :: Either ErrorInfo Int)
           <*> (eMsgEither i l :: Either ErrorInfo Bool)
           <*> (eMsgEither (toReadable g) l :: Either ErrorInfo [Group])
           <*> (eMsgEither (toReadable h) l :: Either ErrorInfo (Maybe Int))
           <*> (eMsgEither (toReadable m) l :: Either ErrorInfo ModifyRule)
           <*> (eMsgEither (toReadable t) l :: Either ErrorInfo VDataType)
           <*> return (DS (T.pack d) (T.pack b))
           <*> return (T.pack c)
           <*> return (Just $ T.pack p)
           <*> return (T.pack u)
           <*> return (Just $ T.pack f)
           <*> (eMsgEither rule l :: Either ErrorInfo (Rule VariableType))
         l = unwords x

toReadable :: String -> String
toReadable = map (\x -> BL.bool x ' '(':'== x))

toShowable :: String -> String
toShowable = map (\x -> BL.bool x ':'(' '== x))

eMsgEither :: (Read a, Show a) => String -> String -> Either ErrorInfo a
eMsgEither o l = B.first (<> msg) $ readEither o
   where msg = ": [ line ] " <> l <> ", [ detail ] " <> show o
--
--
readEnv :: String -> IO (Either String (Env VariableType))
readEnv path = do
   ctx <- readFile path
   return (readEither ctx)
