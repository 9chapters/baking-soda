{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE DeriveDataTypeable #-}

module BakingSoda.Crawler where

import BakingSoda.Db
import BakingSoda.UtilTH
import BakingSoda.DBModels

import Control.Concurrent
import Control.Exception
import Control.Monad

import Data.Typeable
import Data.Data
import Data.List
import Data.Monoid
import Data.Digest.Pure.MD5 as DP
import Data.Time
import Data.Time.Clock
import Data.Time.Format

import qualified Database.Groundhog.Postgresql as G
import qualified Data.Pool as P
import qualified Data.Maybe as M
import qualified Data.Bool as BL
import qualified Data.Aeson as A
import qualified Data.UUID as UD
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.UTF8 as B
import qualified Data.ByteString.Lazy.UTF8 as BU
import qualified Data.HashMap.Strict as H
import qualified System.Random as R
import System.IO

import Text.StringTemplate
import Text.StringTemplate.GenericStandard

import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Network.HTTP.Types.Status (statusCode, ok200)

import Prelude hiding (succ)

class Iso a where
    zero :: a -> a
    succ :: a -> a

data Seed a where
    Gain :: (ToSElem a, Iso a, Show a)
         => String  -- ^ var name
         -> a       -- ^ var
         -> String  -- ^ url template
         -> Seed a

grow :: (ToSElem a, Iso a, Show a, Stringable b) => Seed a -> [ (a, b) ]
grow (Gain n v u) = grow' n v $ newSTMP u
       where grow' :: (ToSElem a, Iso a, Stringable b) => String -> a -> StringTemplate b -> [ (a, b) ]
             grow' n v s = (v, render $ setAttribute n v s) : grow' n (succ v) s

crawl :: Int                       -- ^ retry
      -> [ B.ByteString ]          -- ^ url
      -> IO [ B.ByteString ]
crawl n [] = return []
crawl n xs = do
    let protocol = BU.take 5 (head xs) == "https"
    let ms = BL.bool defaultManagerSettings tlsManagerSettings protocol
    m <- newManager ms
    mapM (req m n . B.toString) xs
    where req :: Manager -> Int -> String -> IO B.ByteString
          req m 0 s = return mempty
          req m n s = do
            request <- parseRequest s
            responseE <- try $ httpLbs request m
            putStr $ "reuqest " <> s
            case responseE :: Either HttpException (Response B.ByteString) of
              Right response -> do
                if responseStatus response == ok200 then do
                   putStrLn " .. success"
                   return $ responseBody response
                else do
                   putStrLn " .. failure, retrying again"
                   req m (n - 1) s
              Left ex -> do
                putStrLn " .. failure, retrying again"
                putStrLn $ displayException ex
                req m (n - 1) s

storeSnapshot :: (Iso a, ToSElem a, Show a, Read a, Eq a)
              => Int     -- ^ retry
              -> Seed a  -- ^ seed
              -> Int     -- ^ temp for resps
              -> Int     -- ^ trunc for inserting
              -> Int     -- ^ sleep when get full data
              -> Int     -- ^ sleep when lost data in this trunc
              -> String  -- ^ source name
              -> String  -- ^ connect string
              -> String  -- ^ connect string for read only
              -> IO ()
storeSnapshot n s r t d1 d2 id cs csr = do
    let urls = grow s
    pool <- connDb cs 3
    poolRead <- connDb csr 3
    buildCrowlerModel pool
    truncCrawl n urls r t d1 d2 id pool poolRead
    where truncCrawl :: (ToSElem a, Iso a, Show a, Read a, Eq a)
              => Int                     -- ^ retry
              -> [ (a, B.ByteString) ]   -- ^ url
              -> Int                     -- ^ temp for resps
              -> Int                     -- ^ trunc for inserting
              -> Int                     -- ^ sleep when get full data
              -> Int                     -- ^ sleep when lost data in this trunc
              -> String                  -- ^ source name
              -> P.Pool G.Postgresql
              -> P.Pool G.Postgresql
              -> IO ()
          truncCrawl n u 0 t d1 d2 id pool poolRead = return ()
          truncCrawl n u r t d1 d2 id pool poolRead = do
              let (f, b) = splitAt t u
              let (base, urls) = unzip f
              putStrLn $ "checking seed: " <> show (head base) <> " ~ " <> show t

              cf <- getInsertList poolRead base

              let f' = filter ((`elem` cf) . fst ) f
              let (base', urls') = unzip f'
              let identities = map show base'
              resps <- crawl n urls'

              let md5Digests = map DP.md5 resps
              -- TODO
              -- level2 check the online/db data via md5
              -- yes: remove from the list
              -- no: stop

              let rawData = resps
              let times = repeat getCurrentTime
              let sourceName = repeat id

              let jsonData :: [ Maybe A.Value ]
                  jsonData = map (A.decodeStrict . B.toStrict) resps

              uuidJson <- mapM (addUuid . (toObject <$>)) jsonData

              let md5s = map show md5Digests
                  (identities', rawData', uuidJson', md5s')
                       = unzip4
                       $ filter (\(x,y,z,w) -> M.isJust z)
                       $ zip4 identities rawData uuidJson md5s

              let pData = map (M.maybe "" A.encode) uuidJson'
              let rds_ = map ($(uncurryN 5) RawData) $ zip5 identities' sourceName rawData' pData md5s'
              rds  <- timeForData rds_ times

              -- TODO: maybe pass https call to this func
              storeRowData t pool rds
              putStrLn "done"
              hFlush stdout

              threadDelay d1
              threadDelay d1
              let fullDatas = length f' == length identities'
              let nextUrl = BL.bool u b fullDatas

              unless fullDatas $ do
                putStrLn "retrying requesting this trunck ..."
                threadDelay d2

              hFlush stdout
              truncCrawl n nextUrl r t d1 d2 id pool poolRead
              --TODO: new seed?

timeForData :: [ t -> d ] -> [ IO t ]-> IO [ d ]
timeForData [] _ = return []
timeForData _ [] = return []
timeForData (f:fs) (x:xs) = (:) <$> fmap f x <*> timeForData fs xs

toObject :: A.Value -> A.Value
toObject v@(A.Object _) = v
toObject v = A.toJSON $ H.fromList [("_value" :: T.Text, v)]

addUuid :: Maybe A.Value -> IO (Maybe A.Value)
addUuid (Just vu) = do
    let addUuid'
          :: UD.UUID  -- ^ parent UUID
          -> A.Value
          -> IO A.Value
        addUuid' u v@(A.Object o) = do
          uuid <- R.randomIO
          A.toJSON
            . H.insert "uuid" (A.toJSON $ UD.toText uuid)
            . H.insert "parent_uuid" (A.toJSON $ UD.toText u)
            <$> traverse (addUuid' uuid) o
        addUuid' u v@(A.Array a) = A.toJSON <$> traverse (addUuid' u) a
        addUuid' u v@(A.String t) = return v
        addUuid' u v@(A.Number s) = return v
        addUuid' u v@(A.Bool b) = return v
        addUuid' u v@A.Null = return v
    o <- R.randomIO
    Just <$> addUuid' o vu
addUuid Nothing = return Nothing

