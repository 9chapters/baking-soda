{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Test.Parser where

import BakingSoda

import qualified Data.Aeson as A
import qualified Data.HashMap.Strict as H
import qualified Data.Vector as V

modifyRule1 :: ModifyRule
modifyRule1 = Expand

stopRule1 :: [ Rule () ]
stopRule1 = []

json1 :: A.Value
json1 = A.Object
         $ H.fromList
         [ ("testValue",  A.String "test1")
         , ("testArray",  A.Array $ V.fromList [ A.String "test2", A.String "test3"  ] )
         , ("testValue1",  A.String "test4")
         ]

modelEnv1 :: ModelEnv VariableType
modelEnv1 = ModelEnv $ H.fromList [("cm9vdC8=", H.fromList [(0, VArray Nothing)])]

rule1 :: Rule VariableType
rule1 = Rule "" "testValue" $ Target $ VText Nothing

rule2 :: Rule VariableType
rule2 = Rule "" "testArray" $ Cat "cm9vdC8=" 0 $ Target $ VText Nothing

rule3 :: Rule VariableType
rule3 = Rule "" "testValue1" $ Target $ VText Nothing

tableField1 :: TableField
tableField1 = TableField
  { fieldName = "field name1"
  , fieldType = "bigint"
  , fieldIsIndex = False
  , fieldModifyRule = modifyRule1
  , fieldRule = [rule1, rule2]
  }

tableField2 :: TableField
tableField2 = TableField
  { fieldName = "field name2"
  , fieldType = "type"
  , fieldIsIndex = True
  , fieldModifyRule = modifyRule1
  , fieldRule = [rule1, rule3]
  }

table1 :: Table
table1 = Table
  { tableName = "table name"
  , tablefield
     = H.singleton (Group 0) [tableField1, tableField2]
  }
